import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import AllUser from "../views/AllUser.vue";
import Vuex from "../views/Vuex.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },

  {
    path: "/showuser",
    name: "showuser",
    component: AllUser
  },

  {
    path: "/vuex",
    name: "vuex",
    component: Vuex
  }

  // {
  //   path: "/login",
  //   name: "loginuser"
  //   component: Login
  // },
  // {
  // path: '/comment',
  // name: 'comment',
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  // component: () => import(/* webpackChunkName: "takis" */ '../views/Takis.vue')
  // }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
